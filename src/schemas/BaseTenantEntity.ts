import { Column } from 'typeorm';

export default class BaseTenantEntity {
	@Column({ default: () => `current_setting('rls.tenant_id')` })
	tenantId!: string;
}
