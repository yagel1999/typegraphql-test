import { Field, InputType, Int, ObjectType } from 'type-graphql';
import { Entity, Column, PrimaryGeneratedColumn, DeepPartial } from 'typeorm';
import { Status } from '../../common/models';
import { BaseTenantEntity } from '../entities';

@ObjectType()
@Entity()
export default class Todo extends BaseTenantEntity {
	@Field()
	@PrimaryGeneratedColumn('uuid')
	id!: string;

	@Field(() => Int, { nullable: true })
	@Column('int', { nullable: true })
	owningUserId?: number;

	@Field()
	@Column()
	title!: string;

	@Field(() => Int)
	@Column('int')
	status?: number;
}

@InputType()
export class CreateTodoInput implements DeepPartial<Todo> {
	@Field()
	title!: string;

	@Field(() => Int, { nullable: true })
	owningUserId?: number;

	@Field(() => Int, {
		defaultValue: Status.Active,
		nullable: true
	})
	status!: Status;
}

@InputType()
export class UpdateTodoInput implements DeepPartial<Todo> {
	@Field()
	id!: string;

	@Field({ nullable: true })
	title?: string;

	@Field(() => Int, { nullable: true })
	status?: Status;

	@Field(() => Int, { nullable: true })
	owningUserId?: number;
}
