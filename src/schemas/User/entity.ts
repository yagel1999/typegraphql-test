import {
	Entity,
	Column,
	PrimaryGeneratedColumn,
	BaseEntity,
	DeepPartial
} from 'typeorm';
import { Field, InputType, Int, ObjectType } from 'type-graphql';

@ObjectType()
@Entity()
export default class User extends BaseEntity {
	@Field(() => Int)
	@PrimaryGeneratedColumn()
	id!: number;

	@Field()
	@Column()
	firstName!: string;

	@Field()
	@Column()
	lastName!: string;
}

@InputType()
export class CreateUserInput implements DeepPartial<User> {
	@Field()
	firstName!: string;

	@Field()
	lastName!: string;
}

@InputType()
export class UpdateUserInput implements DeepPartial<User> {
	@Field(() => Int)
	id!: number;

	@Field({ nullable: true })
	firstName?: string;

	@Field({ nullable: true })
	lastName?: string;
}
