import { Logger, createLogger, transports, format } from "winston";

export default class CustomLogger {
    public static logger: Logger = createLogger({
        transports: [
            new transports.Console()
        ],
        format: format.json(),
        level: 'info'
    });
}
